/*
 *  Copyright (C) 2020 Albert Astals Cid <aacid@kde.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chmk.h"

#include <QApplication>
#include <QWebEngineUrlScheme>

int main(int argc, char **argv)
{
    QWebEngineUrlScheme scheme("ms-its");
    scheme.setSyntax(QWebEngineUrlScheme::Syntax::Host);
    scheme.setFlags(QWebEngineUrlScheme::LocalAccessAllowed	);
    QWebEngineUrlScheme::registerScheme(scheme);

    QApplication a(argc, argv);

    Chmk v;
    v.show();
    v.resize(700, 500);

    v.openFile(argv[1]);

    return a.exec();
}

/*
 *  Copyright (C) 2020 Albert Astals Cid <aacid@kde.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chmk.h"

#include "lib/ebook_chm.h"

#include <QBuffer>
#include <QDomElement>
#include <QHBoxLayout>
#include <QWebEngineView>
#include <QWebEngineProfile>
#include <QWebEngineUrlSchemeHandler>
#include <QWebEngineUrlRequestJob>

class MSITSSchemeHandler : public QWebEngineUrlSchemeHandler
{
public:
    void requestStarted(QWebEngineUrlRequestJob *request) override
    {
        QBuffer *device = new QBuffer();
        if (!m_file->getFileContentAsBinary(device->buffer(), request->requestUrl()))
        {
            qDebug() << "getBinaryContent failed" << request->requestUrl();
            request->fail(QWebEngineUrlRequestJob::UrlNotFound);
            return;
        }

        request->reply("text/html", device);
        connect(request, &QObject::destroyed, device, &QObject::deleteLater);
    }

    void setFile(EBook_CHM *file)
    {
        m_file = file;
    }

private:
    EBook_CHM *m_file;
};

Chmk::Chmk() : QWidget()
{
    QHBoxLayout *lay = new QHBoxLayout(this);
    m_view = new QWebEngineView(this);
    lay->addWidget(m_view);
}

void Chmk::openFile(const QString &filePath)
{
    MSITSSchemeHandler *handler = new MSITSSchemeHandler();
    QWebEngineProfile::defaultProfile()->installUrlSchemeHandler("ms-its", handler);

    EBook_CHM *file = static_cast<EBook_CHM *>(EBook::loadFile( filePath ));
    handler->setFile(file);

    // TODO BEGIN copied from Okular, needs cleaning
    QList< EBookTocEntry > topics;
    file->getTableOfContents(topics);

    // fill m_docSyn
    QDomDocument m_docSyn;
    QMap<QString, int> m_urlPage;
    QVector<QString> m_pageUrl;
    QMap<int, QDomElement> lastIndentElement;
    QMap<QString, int> tmpPageList;
    int pageNum = 0;

    for (const EBookTocEntry &e : qAsConst(topics))
    {
        QDomElement item = m_docSyn.createElement(e.name);
        if (!e.url.isEmpty())
        {
            QString url = e.url.toString();
            item.setAttribute(QStringLiteral("ViewportName"), url);
            if(!tmpPageList.contains(url))
            {//add a page only once
                tmpPageList.insert(url, pageNum);
                pageNum++;
            }
        }
        item.setAttribute(QStringLiteral("Icon"), e.iconid);
        if (e.indent == 0) m_docSyn.appendChild(item);
        else lastIndentElement[e.indent - 1].appendChild(item);
        lastIndentElement[e.indent] = item;
    }

    // fill m_urlPage and m_pageUrl
    QList<QUrl> pageList;
    file->enumerateFiles(pageList);
    const QUrl home = file->homeUrl();
    if (home.path() != QLatin1String("/"))
        pageList.prepend(home);
    m_pageUrl.resize(pageNum);

    for (const QUrl &qurl : qAsConst(pageList))
    {
        QString url = qurl.toString();
        const QString urlLower = url.toLower();
        if (!urlLower.endsWith(QLatin1String(".html")) && !urlLower.endsWith(QLatin1String(".htm")))
            continue;

        int pos = url.indexOf (QLatin1Char(('#')));
        // insert the url into the maps, but insert always the variant without the #ref part
        QString tmpUrl = pos == -1 ? url : url.left(pos);

        // url already there, abort insertion
        if (m_urlPage.contains(tmpUrl)) continue;

        int foundPage = tmpPageList.value(tmpUrl, -1);
        if (foundPage != -1 ) {
            m_urlPage.insert(tmpUrl, foundPage);
            m_pageUrl[foundPage] = tmpUrl;
        } else {
            //add pages not present in toc
            m_urlPage.insert(tmpUrl, pageNum);
            m_pageUrl.append(tmpUrl);
            pageNum++;
        }
    }
    // TODO END copied from Okular, needs cleaning

    m_view->setUrl(m_pageUrl[0]);
}
